<?php

require __DIR__ . "/vendor/autoload.php";

$app = new \Sample\MyService();
$app->mountApis();
$app->run();
