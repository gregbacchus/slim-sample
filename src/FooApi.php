<?php
namespace Sample;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class FooApi implements \Common\Api
{
    public function addRoutes(\Slim\App $router)
    {
        $router->get('', [$this, 'get']);
        $router->get('/{name}', [$this, 'getName']);
    }

    public function get(Request $request, Response $response, array $args)
    {
        $response->getBody()->write('Hello world!');

        return $response;
    }

    public function getName(Request $request, Response $response, array $args)
    {
        $name = $args['name'];
        $response->getBody()->write("Hello, $name");

        return $response;
    }
}
