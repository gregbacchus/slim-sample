<?php
namespace Sample;

class MyService extends \Common\Service
{
    public function mountApis()
    {
        $this->mount('/test', new \Sample\TestApi());
        $this->mount('/foo', new \Sample\FooApi());
    }
}
