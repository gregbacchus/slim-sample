<?php
namespace Sample;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class TestApi implements \Common\Api
{
    public function addRoutes(\Slim\App $router)
    {
        $router->get('', [$this, 'get']);
        $router->post('', [$this, 'post']);
    }

    public function get(Request $request, Response $response, array $args)
    {
        $response->getBody()->write('Testing');

        return $response;
    }

    public function post(Request $request, Response $response, array $args)
    {
        $response->getBody()->write('Posted');

        return $response;
    }
}
